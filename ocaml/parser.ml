open ASD
open Token

(* p? *)
let opt p = parser
  | [< x = p >] -> Some x
  | [<>] -> None

(* p* *)
let rec many p = parser
  | [< x = p; l = many p >] -> x :: l
  | [<>] -> []

(* p+ *)
let some p = parser
  | [< x = p; l = many p >] -> x :: l

(* p (sep p)* *)
let rec list1 p sep = parser
  | [< x = p; l = list1_aux p sep >] -> x :: l
and list1_aux p sep = parser
  | [< _ = sep; l = list1 p sep >] -> l
  | [<>] -> []

(* (p (sep p)* )? *)
let list0 p sep = parser
  | [< l = list1 p sep >] -> l
  | [<>] -> []


let rec program = parser
  | [< p = some function_or_proto; _ = Stream.empty ?? "Unexpected input at the end of the file" >] -> p

and function_or_proto = parser
  | [< 'PROTO_KW; p = proto >] -> Proto p
  | [< 'FUNC_KW; (t, f, params) = proto; body = statement t >] -> Function ((t, f, params), body)
and proto = parser
  | [< t = type_qual; 'IDENT f; 'LP ?? "'(' expected in function declaration";
       params = list0 param comma;
       'RP ?? "Missing ')' at the end of function parameter list" >] -> (t, f, params)
  | [<>] -> failwith "Expected type qualifier after FUNC/PROTO keyword"
and type_qual = parser
  | [< 'INT_KW >] -> Type_Int
  | [< 'VOID_KW >] -> Type_Void
and param = parser
  | [< 'IDENT i >] -> i

and statement func_ret_type = parser
  | [< 'IDENT i; res = right_of_id_statement i >] -> res
  | [< 'LB; d = declaration; s = some (statement func_ret_type); 'RB ?? "Missing '}' at end of block" >] -> Block (d, s)
  | [< 'IF_KW; e = expression; 'THEN_KW ?? "THEN keyword expected"; t = statement func_ret_type;
       else_opt = opt (parser [< 'ELSE_KW; f = statement func_ret_type >] -> f);
       'FI_KW ?? "FI keyword expected" >] -> IfThenElse (e, t, else_opt)
  | [< 'WHILE_KW; e = expression; 'DO_KW ?? "DO keyword expected"; s = statement func_ret_type;
       'DONE_KW ?? "DONE keyword expected" >] -> While (e, s)
  | [< 'RETURN_KW; e = expression >] -> Return (func_ret_type, e)
  | [< 'PRINT_KW; args = list1 print_item comma >] -> Print args
  | [< 'READ_KW; args = list1 read_item comma >] -> Read args
and right_of_id_statement id = parser
  | [< 'LP; args = list0 expression comma; 'RP ?? "Missing ')' at end of function call" >] -> FunctionCall (id, args)
  | [< 'LC; e = expression; 'RC ?? "Missing ']' in array access"; res = assignment (LValueArrayElem (id, e)) >] -> res
  | [< res = assignment (LValueIdent id) >] -> res

and assignment lval = parser
  | [< 'ASSIGN; e = expression >] -> Assignment (lval, e)

and lvalue = parser
  | [< 'IDENT i; res = right_of_id_lvalue i >] -> res
and right_of_id_lvalue id = parser
  | [< 'LC; e = expression; 'RC ?? "Missing ']' in array access" >] -> LValueArrayElem (id, e)
  | [<>] -> LValueIdent id

and expression = parser
  | [< e1 = factor; e = expression_aux e1 >] -> e
and expression_aux e1 = parser
  | [< 'PLUS; e2 = factor; e = expression_aux (AddExpression (e1, e2)) >] -> e
  | [< 'MINUS; e2 = factor; e = expression_aux (SubstExpression (e1, e2)) >] -> e
  | [<>] -> e1

and factor = parser
  | [< e1 = primary; e = factor_aux e1 >] -> e
and factor_aux e1 = parser
  | [< 'MUL; e2 = primary; e = factor_aux (MulExpression (e1, e2)) >] -> e
  | [< 'DIV; e2 = primary; e = factor_aux (DivExpression (e1, e2)) >] -> e
  | [<>] -> e1

and primary = parser
            | [< 'INTEGER x >] -> IntegerExpression x
            | [< 'LP; e = expression; 'RP >] -> e
            | [< 'IDENT i; res = right_of_id_expression i >] -> res
and right_of_id_expression id = parser
                              | [< 'LP; args = list0 expression comma; 'RP ?? "Missing ')' at end of function call" >] -> ExprFunctionCall (id, args)
                              | [< 'LC; e = expression; 'RC ?? "Missing ']' in array access" >] -> ArrayElemExpr (id, e)
                              | [<>] -> VarExpression id

and declaration = parser
                | [< dd = many declaration_list >] -> List.flatten dd
and declaration_list = parser
                     | [< 'INT_KW; l = list1 declaration_elem comma >] -> l
and declaration_elem = parser
                     | [< 'IDENT i; res = right_of_id_declaration i >] -> res
and right_of_id_declaration id = parser
                               | [< 'LC; 'INTEGER n; 'RC ?? "Missing ']' in array access" >] -> ArrayDeclaration (id, n)
                               | [<>] -> VariableDeclaration id

and print_item = parser
               | [< 'TEXT text >] -> PrintText text
               | [< e = expression >] -> PrintExpression e
and read_item = parser
              | [< x = lvalue >] -> x

and comma = parser
          | [< 'COM >] -> ()
