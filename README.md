# PDS : Rapport

Nous avons fait le choix de ne pas implémenter le *pretty-printer* car il n'est
pas nécessaire au bon fonctionnement du compilateur VSL+.

## ASD

En plus des types courants, on remarque l'introduction de deux types
particuliers: `lvalue` et `cond`. Les `lvalue`s représentent les variables qui
sont assignables. Le type `cond` représente une condition: il contient une
expression et deux labels `if_true` et `if_false`. Si l'expression est évaluée
à faux un branchement sera effectué vers le label `if_false`, `if_true` sinon.

Le constructeur `Type_Array` permet de donner le type d'un tableau et de
sauvegarder sa taille.

## Parser

Le parser dispose d'une gestion simple des erreurs de syntaxe grâce aux
fonctionnalités des *streams parsers*.

Pour gérer les priorités d'évaluation des opérateurs dans les expressions, nous
avons séparé le parsing des expressions en plusieurs étages qui définissent les
priorités. 

Un point remarquable est le parsing des identificateurs car il ne suffit pas de
parser un identificateur seul. En effet, un identificateur peut être soit une
variable, soit un tableau, soit une fonction. Comme le type d'un identificateur
est défini par ce qui se trouve à sa droite, nous avons fait le choix de
séparer le parsing de l'identificateur en lui-même et des éventuels crochets
et/ou parenthèses qui le suivent.

On remarque également que nous avons fait le choix de ne pas traiter les
tableaux passés en paramètre de fonction car cette fonctionnalité ne fait pas
partie de la spécification donnée par le fichier `vsl.pdf`.

## LLVM

Chaque constructeur du type `llvm_instr` correspond à une seule instruction
dans la représentation intermédiaire LLVM. 

On peut noter la présence du constructeur `FuncDefEnd` qui permet de
représenter la fin de la définition d'une fonction, i.e. le caractère `}`.

Contrairement au type `typ` de l'ASD, `llvm_type` ne comporte de constructeur
pour les tableaux. En effet, la nature de l'identificateur manipulé par les
instructions LLVM est contenue dans les constructeur du type `llvm_instr`.

## Génération de code

Les sections suivantes présentent brièvement le fonctionnement des principales
fonctions de génération d'IR LLVM.

### Fonctions et prototypes

Dans le cas d'un prototype VSL+ on rajoute le symbole de la fonction ainsi
déclarés à la table des symboles courante sans générer de code.

Pour une définition on ajoute la fonction à la table des symbole, on génère le
code LLVM associé à son prototype puis le code de son corps. On vérifie au
passage que la définition correspond bien à l'éventuelle déclaration préalable
de la fonction.

Afin de pouvoir utiliser les paramètres des fonctions dans leur corps, il est
nécessaire d'en créer une copie locale. Pour ce faire, nous renommons les
paramètre en leur ajoutant un préfixe distinctif. Les copies locales ont le
nom des paramètres originaux.

Lors de la définition d'une fonction, on veille à insérer le `}` en toute
fin du corps.

Afin d'assurer que `clang` compilera bien le fichier LLVM généré, on force la
présence d'une instruction de retour à la fin de chaque fonction.

### Statements

On utilise avantageusement les fonctions de haut niveau fournies par le module
OCaml `List`.

Dans le cas des boucles et des conditionnelles, on utilise le type `cond`
introduis dans l'ASD. La branche `else` d'une conditionnelle étant optionnelle,
nous utilisons le type OCaml `option`.

## Auteurs

Jean Jouve et Jean-Michel Gorius
